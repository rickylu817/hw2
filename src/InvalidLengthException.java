

/**
 * The InvalidLengthException class is meant to be a custom Exception class
 * that reports the exception of the user inputting an invalid length for the
 * Song
 * 
 */
public class InvalidLengthException extends Exception
{
    public InvalidLengthException(String msg)
    {
        super(msg);
    }
}